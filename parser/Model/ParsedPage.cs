﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Parser.model
{
    [Table("ParsedPages")]
    public class ParsedPage
    {
        [Key]
        public string Url { get; set; }

        public string Title { get; set; }

        [Column(TypeName = "ntext")]
        public string Body { get; set; }

        public double? Price { get; set; }
    }
}

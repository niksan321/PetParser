﻿using System.Data.Entity;

namespace Parser.model
{
    /// <summary>
    /// Контекст работы с бд
    /// </summary>
    public class LocalDbContext: DbContext
    {
        /// <summary>
        /// Распарсенные страницы
        /// </summary>
        public DbSet<ParsedPage> ParsedPages { get; set; } 
    }
}

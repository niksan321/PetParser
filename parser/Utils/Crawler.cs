﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Abot.Crawler;
using Abot.Poco;
using System.Threading;
using System.Net;
using HtmlAgilityPack;
using Parser.model;

namespace Parser
{
    /// <summary>
    /// Загружает и распарсивает данные сайта
    /// </summary>
    public class CrawlerAndParser : IDisposable
    {
        #region Константы
        /// <summary>
        /// Какой сайт загружаем
        /// </summary>
        const string baseUrl = @"http://www.zoo-zoo.ru";

        /// <summary>
        /// С какой строки должна начинатся ссылка с нужными для парсинга данными
        /// </summary>
        const string parseUrl = baseUrl + "/messages";

        /// <summary>
        /// Максимальное количество потоков для загрузчика
        /// </summary>
        const int MAX_CONCURENT_THREADS = 10;

        /// <summary>
        /// Время ожидания данных от сайта, сек
        /// </summary>
        const int HTTP_REQUEST_TIMEOUT_IN_SECONDS = 10;

        /// <summary>
        /// Каким браузером прикидывается загрузчик
        /// </summary>
        const string USER_AGENT_STRING = "Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko";

        /// <summary>
        /// Положительное решение
        /// </summary>
        static readonly CrawlDecision crawlDecisionTrue = new CrawlDecision { Allow = true };

        /// <summary>
        /// Отрицательное решение
        /// </summary>
        static readonly CrawlDecision crawlDecisionFalse = new CrawlDecision { Allow = false };
        #endregion

        #region Переменные
        /// <summary>
        /// Загрузчиг Web страниц
        /// </summary>
        PoliteWebCrawler crawler;

        /// <summary>
        /// Финальный результат работы загрузчика Web страниц
        /// </summary>
        CrawlResult crawlerResult;

        /// <summary>
        /// Главное окно программы для
        /// </summary>
        MainWindow mainWindow;

        /// <summary>
        /// Настройки загрузчика
        /// </summary>
        CrawlConfiguration crawlerConfig;

        /// <summary>
        /// Токен для отмены работы загрузчика
        /// </summary>
        CancellationTokenSource cancellationTokenSource;

        /// <summary>
        /// Загрузчик работает?
        /// </summary>
        public bool isWorking; 
        #endregion

        #region События
        /// <summary>
        /// Новые данные добавлены
        /// </summary>
        public event EventHandler PageAdded = (o, e) => { };

        /// <summary>
        /// Данные обновлены
        /// </summary>
        public event EventHandler PageUpdated = (o, e) => { };

        /// <summary>
        /// Ошибка загрузки данных
        /// </summary>
        public event EventHandler PageError = (o, e) => { };
        #endregion

        #region Методы
        public CrawlerAndParser(MainWindow main)
        {
            mainWindow = main;
            MakeCrawlerConfig();
        }

        /// <summary>
        /// Настройки загрузчика
        /// </summary>
        void MakeCrawlerConfig()
        {
            crawlerConfig = new CrawlConfiguration();
            crawlerConfig.MaxConcurrentThreads = MAX_CONCURENT_THREADS;
            crawlerConfig.HttpRequestTimeoutInSeconds = HTTP_REQUEST_TIMEOUT_IN_SECONDS;
            crawlerConfig.UserAgentString = USER_AGENT_STRING;
        }

        /// <summary>
        /// Инициализация загрузчика перед использованием
        /// </summary>
        public void Init()
        {
            if (cancellationTokenSource != null)
            {
                cancellationTokenSource.Dispose();
            }
            cancellationTokenSource = new CancellationTokenSource();
            crawler = new PoliteWebCrawler(crawlerConfig, null, null, null, null, null, null, null, null);
            crawler.PageCrawlCompletedAsync += crawler_ProcessPageCrawlCompleted;
            crawler.ShouldDownloadPageContent(ShouldDownloadPageContent);
            crawler.ShouldCrawlPageLinks(ShouldCrawlPageLinks);
        }

        /// <summary>
        /// Нужна ли нам данная ссылка
        /// </summary>
        CrawlDecision ShouldCrawlPageLinks(CrawledPage crawledPage, CrawlContext crawlContext)
        {
            if (crawledPage.IsInternal && crawledPage.Uri.AbsoluteUri.StartsWith(baseUrl))
            {
                return crawlDecisionTrue;
            }
            return crawlDecisionFalse;
        }

        /// <summary>
        /// Нужна ли нам данная страница
        /// </summary>
        CrawlDecision ShouldDownloadPageContent(CrawledPage crawledPage, CrawlContext crawlContext)
        {
            if (crawledPage.Uri.AbsoluteUri.StartsWith(baseUrl))
            {
                return crawlDecisionTrue;
            }
            return crawlDecisionFalse;
        }

        /// <summary>
        /// Страница скачена
        /// </summary>
        void crawler_ProcessPageCrawlCompleted(object sender, PageCrawlCompletedArgs e)
        {
            var crawledPage = e.CrawledPage;

            if (crawledPage.WebException != null || crawledPage.HttpWebResponse.StatusCode != HttpStatusCode.OK)
            {
                PageError(crawledPage.Uri.AbsoluteUri, null);
            }
            else
            {
                if (crawledPage.Uri.AbsoluteUri.StartsWith(parseUrl))
                {
                    var parsedPage = ParsePage(crawledPage.HtmlDocument, crawledPage.Uri.AbsoluteUri);
                    SaveToDb(parsedPage);
                }
            }
        }

        /// <summary>
        /// Распарсиваем скаченную страницу
        /// </summary>
        /// <param name="doc">скаченная страница</param>
        /// <param name="url">ссылка на эту страницу</param>
        /// <returns>Распарсенную сущность</returns>
        private ParsedPage ParsePage(HtmlDocument doc, string url)
        {
            var page = new ParsedPage();
            page.Title = doc.DocumentNode.SelectNodes("//title")?.FirstOrDefault().InnerText.Trim();
            page.Body = doc.DocumentNode.SelectNodes("//footer")?.FirstOrDefault().InnerText.Trim();
            page.Url = url;

            var price = doc.DocumentNode
                .SelectNodes("//div[@class='panel panel-price-coll']/div[@class='pr-block']")
                ?.FirstOrDefault()
                .InnerText
                .OnlyNumbers();
            try
            {
                page.Price = double.Parse(price);
            }
            catch { }

            return page;
        }

        /// <summary>
        /// Запись сущности в бд
        /// </summary>
        public void SaveToDb(ParsedPage page)
        {
            var db = new LocalDbContext();
            var dbPage = db.ParsedPages.FirstOrDefault(x => x.Url == page.Url);
            if (dbPage == null)
            {
                db.ParsedPages.Add(page);
                db.SaveChanges();
                PageAdded(page.Url, null);
            }
            else
            {
                page.Url = dbPage.Url;
                dbPage = page;
                db.SaveChanges();
                PageUpdated(page.Url, null);
            }
        }

        /// <summary>
        /// Запуск загрузчика
        /// </summary>
        /// <returns></returns>
        public async Task Start()
        {
            isWorking = true;
            crawlerResult = null;
            var url = baseUrl;
            await Task.Factory.StartNew(() =>
            {
                crawlerResult = crawler.Crawl(new Uri(url), cancellationTokenSource);
            });
            isWorking = false;
        }

        /// <summary>
        /// Остановка загрузчика
        /// </summary>
        /// <returns></returns>
        public async Task Stop()
        {
            cancellationTokenSource.Cancel();
            while (crawlerResult == null)
            {
                await Task.Factory.StartNew(() => Thread.Sleep(1000));
            }
            isWorking = false;
        }

        public void Dispose()
        {
            if (cancellationTokenSource != null)
            {
                cancellationTokenSource.Dispose();
            }
        } 
        #endregion
    }
}

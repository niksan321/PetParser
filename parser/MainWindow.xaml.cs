﻿using Parser.model;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Parser
{
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Контекст Бд
        /// </summary>
        public LocalDbContext db { get; set; }

        /// <summary>
        /// Загрузчик
        /// </summary>
        public CrawlerAndParser crawler { get; set; }

        /// <summary>
        /// Результаты поиска
        /// </summary>
        public ObservableCollection<ParsedPage> SearchResults { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            db = new LocalDbContext();
            crawler = new CrawlerAndParser(this);
            lDbCount.Content = db.ParsedPages.Count();
            SearchResults = new ObservableCollection<ParsedPage>();
            dgSearchResults.ItemsSource = SearchResults;
            bCrawlerStart_Click(null, null);
            crawler.PageAdded += Crawler_PageAdded;
            crawler.PageUpdated += Crawler_PageUpdated;
            crawler.PageError += Crawler_PageError;
        }

        /// <summary>
        /// Загрузчик не смог загрузить страницу
        /// </summary>
        /// <param name="sender">Url адрес страницы</param>
        private void Crawler_PageError(object sender, EventArgs e)
        {
            Log($"Ошибка загрузкси страницы {(string)sender}");
        }

        /// <summary>
        /// Загрузчик обновил данные в бд
        /// </summary>
        /// <param name="sender">Url адрес страницы</param>
        private void Crawler_PageUpdated(object sender, EventArgs e)
        {
            Log($"Страница обновлена {(string)sender}");
        }

        /// <summary>
        /// Загрузчик добавил новые данные в бд
        /// </summary>
        /// <param name="sender">Url адрес страницы</param>
        private void Crawler_PageAdded(object sender, EventArgs e)
        {
            Log($"Страница добавлена {(string)sender}");
            UpdateDbCount();
        }

        /// <summary>
        /// Старт/стоп загрузки данных
        /// </summary>
        private async void bCrawlerStart_Click(object sender, RoutedEventArgs e)
        {
            if (crawler.isWorking)
            {
                await StopCrawling();
            }
            else
            {
                await StartCrawling();
            }
        }

        /// <summary>
        /// Запуск загрузки страниц
        /// </summary>
        private async Task StartCrawling()
        {
            rtbLog.Document.Blocks.Clear();
            rtbLog.AppendText("Загрузка данных с сайта www.zoo-zoo.ru начата" + Environment.NewLine);
            rtbLog.AppendText(Environment.NewLine);
            bCrawlerStart.Content = "Остановить";
            crawler.Init();
            await crawler.Start();
            bCrawlerStart.Content = "Запустить";
            rtbLog.AppendText("Загрузка данных с сайта www.zoo-zoo.ru закончена" + Environment.NewLine);
        }

        /// <summary>
        /// Остановка загрузки страниц
        /// </summary>
        private async Task StopCrawling()
        {
            bCrawlerStart.IsEnabled = false;
            bCrawlerStart.Content = "Запустить";
            await crawler.Stop();
            bCrawlerStart.IsEnabled = true;
            rtbLog.AppendText("Загрузка данных с сайта www.zoo-zoo.ru остановлена" + Environment.NewLine);
        }

        /// <summary>
        /// Поиск
        /// </summary>
        private void tbSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                SearchResults.Clear();
                var text = tbSearch.Text.Trim();
                var findedPages = db.ParsedPages
                                .Where(x => x.Title.Contains(text) || x.Body.Contains(text))
                                .ToList();
                findedPages.ForEach(x => SearchResults.Add(x));
            }
        }

        private async void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (crawler.isWorking)
            {
                await crawler.Stop();
            }
            crawler.Dispose();
        }

        /// <summary>
        /// Открытие выбранной страницы в браузере
        /// </summary>
        private void Link_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var source = (TextBlock)e.OriginalSource;
            var uri = new Uri(source.ToolTip.ToString());
            Process.Start(uri.AbsoluteUri);
            e.Handled = true;
        }

        /// <summary>
        /// Обновление UI счётчика записей в бд
        /// </summary>
        void UpdateDbCount()
        {
            App.Current?.Dispatcher.Invoke(() =>
            {
                lDbCount.Content = db.ParsedPages.Count();
            });
        }

        /// <summary>
        /// Вывод текста в UI лог
        /// </summary>
        /// <param name="text"></param>
        void Log(string text)
        {
            App.Current?.Dispatcher.Invoke(() =>
            {
                rtbLog.AppendText(text + Environment.NewLine);
                rtbLog.ScrollToEnd();
            });
        }
    }
}

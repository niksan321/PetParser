﻿using System;
using System.Text;

namespace Parser
{
    public static class StringExtension
    {
        /// <summary>
        /// Возвращает из строки только цифры
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string OnlyNumbers(this string s)
        {
            var sb = new StringBuilder();
            foreach(var c in s)
            {
                if (Char.IsDigit(c))
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }
    }
}

namespace Parser.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ParsedPages",
                c => new
                    {
                        Url = c.String(nullable: false, maxLength: 4000),
                        Title = c.String(maxLength: 4000),
                        Body = c.String(),
                        Price = c.Double(),
                    })
                .PrimaryKey(t => t.Url);
        }
        
        public override void Down()
        {
            DropTable("dbo.ParsedPages");
        }
    }
}

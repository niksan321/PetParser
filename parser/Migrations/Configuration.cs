namespace Parser.Migrations
{
    using model;
    using System.Data.Entity.Migrations;

    /// <summary>
    /// Настройка миграций
    /// </summary>
    internal sealed class Configuration : DbMigrationsConfiguration<LocalDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }
    }
}
